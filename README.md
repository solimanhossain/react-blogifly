# REACT BLOGIFY BY SOLIMAN

<img src="https://gitlab.com/solimanhossain/react-blogifly/-/raw/main/public/preview.png?inline=false" alt="tic-tac-toe" width="250" align="left"/>

### REACT BLOGIFY | [Code](https://gitlab.com/solimanhossain/react-blogifly.git) | [Preview](https://react-blogifly.pages.dev/) |

```
cd react-blogifly
npm install
npm run dev
```

_You can create account or use test account :_

Email: `Soliman2024@test.com`
Password: `Soliman2024@test.com`

---

_Get ***[(API File)](https://gitlab.com/solimanhossain/react-blogifly/-/tree/main/backend-api)*** and start backend API server in http://localhost:3000/ to run this project. Follow [documentation](https://documenter.getpostman.com/view/9649334/2sA2rCU2NA) to manage it properly._

---

<br>

-   These project is done by myself ([Soliman Hossain](https://github.com/solimanhossain/)).

-   Backend API and HTML template are provided by Learn with Sumit ([repo: rnext](https://github.com/Learn-with-Sumit/rnext/)).

-   To run projects clone repo and install [Node.js](https://nodejs.org/en/download/) then run code on cmd or terminal follow url.
